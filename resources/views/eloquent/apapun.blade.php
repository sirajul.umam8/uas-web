<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Belajar Eloquent</title>
</head>
<body>
    <h3>Data pegawai</h3>
    <br/>
    <br/>
    <ul>
        @foreach ($pegawai as $pe)
            <li>{{"Nama : ".$pe->nama.' | Alamat : '.$pe->alamat}}</li>
        @endforeach
    </ul>
    {{-- <table border="1">
        <tr>
            <td>Nama</td>
            <td>Alamat</td>
        </tr>
        @foreach ($pegawai as $pe)
            <tr>
                <td>{{$pe->nama}}</td>
                <td>{{$pe->alamat}}</td>
            </tr>
        @endforeach
    </table> --}}
</body>
</html>