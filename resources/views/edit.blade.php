<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cara membuat CRUD pada laravel</title>
</head>
<body>
    <h2>Edit Pegawai</h2>
    <a href="/pegawai">Kembali</a>
    <br/>
    <br/>
    @foreach ($pegawai2 as $pg)
        <form action="/pegawai/update" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $pg -> pegawai_id }}"><br/>
            Nama <input type="text" name="nama" required="required" value="{{$pg-> pegawai_nama}}"><br/>
            Jabatan <input type="text" name="jabatan" required="required" value="{{$pg-> pegawai_jabatan}}"><br/>
            Umur <input type="text" name="umur" required="required" value="{{$pg-> pegawai_umur}}"><br/>
            Alamat <input type="text" name="alamat" required="required" value="{{$pg-> pegawai_alamat}}"><br/>
            <input type="submit" value="Simpan">
        </form>
    @endforeach
</body>
</html>