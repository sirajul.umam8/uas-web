<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Membuat CRUD pada laravel</title>
    <link rel="stylesheet" type="text/css" href="CSS/app.css ">
</head>
<body>
    <style type="type/css">
        .pagination li{
            float: left;
            list-style-type: none;
            margin: 5px;
        } </style>
    <h3>Data pegawai</h3>
    <p>Cari Data pegawai</p>
    <form action="/pegawai/cari" method="GET">
        <input class="form-control" type="text" name="cari" placeholder="Cari..." value="{{old('temukan')}}">
        <input class="btn btn-primary ml-3" type="submit" value="Cari"><br/>
    </form>
    <a href="/pegawai/tambah">Tambah pegawai baru</a>
    <br/>
    <br/>
    <table border="1">
        <tr>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>Umur</th>
            <th>Alamat</th>
            <th>Opsi</th>
        </tr>
        @foreach ($pegawai as $pgw)
        <tr>
            <td>{{$pgw->pegawai_nama}}</td>
            <td>{{$pgw->pegawai_jabatan}}</td>
            <td>{{$pgw->pegawai_umur}}</td>
            <td>{{$pgw->pegawai_alamat}}</td>
            <td>
                <a href="/pegawai/edit/{{$pgw->pegawai_id}}">Edit</a>
                |
                <a href="/pegawai/hapus/{{$pgw->pegawai_id}}">Hapus</a>
            </td>
        </tr> 
        @endforeach
    </table>
    Halaman : {{$pegawai->currentPage()}} <br/>
    Jumlah Data : {{$pegawai->total()}} <br/>
    Data Per Halaman : {{$pegawai->perPage()}} <br/>

    {{$pegawai->links()}}


</body>
</html>