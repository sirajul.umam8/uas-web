<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cara Membuat CRUD pada laravel</title>
</head>
<body>
    <h3>Data Pegawai</h3>
    <a href="/pegawai">Kembali</a>
    <br/>
    <br/>
    <form action="/pegawai/penyimpanan" method="POST">
        {{csrf_field()}}
        Nama <input type="text" name="nama" required="required"><br/>
        Jabatan <input type="text" name="jabatan" required="required"><br/>
        Umur <input type="text" name="umur" required="required"><br/>
        Alamat <textarea name="alamat" id="" cols="30" rows="10" required="required"></textarea><br/>
        <input type="submit" value="simpan data">
    </form>
</body>
</html>