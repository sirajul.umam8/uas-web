<!-- Menghubungkan dengan template master -->
@extends('master')

<!-- Isi dengan bagian judul halaman -->
<!-- Cara penulisan section yang pendek -->
@section('judul_halaman','Halaman Kontak')

<!-- isi bagian konten -->
<!-- cara penulisan isi section yang panjang -->
@section('konten')
    <p>Ini adalah halaman Kontak</p>

    <table border="1">
        <tr>
            <td>Email</td>
            <td>:</td>
            <td>sirajul.umam6@gmail.com</td>
        </tr>
        <tr>
            <td>Hp</td>
            <td>:</td>
            <td>0819-9955-4012</td>
        </tr>
    </table>
@endsection