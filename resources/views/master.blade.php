<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sistem Template Blade Laravel</title>
</head>
<body>
    <header>
        <h2>Pemalas</h2>
        <nav>
            <a href="/blog2">HOME</a>
            |
            <a href="/blog2/tentang">TENTANG</a>
            |
            <a href="/blog2/kontak">KONTAK</a>
        </nav>
    </header>
    <hr/>
    <br/>
    <br/>
    <!-- Bagian judul halaman blog -->
    <h3>@yield('judul_halaman')</h3>

    <!-- Bagian konten blog -->
    @yield('konten')

    <br/>
    <br/>
    <hr/>

    <footer>
        <p>2022-2023</p>
    </footer>
</body>
</html>