<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('halo', function (){
//     return "Halo, Selamat Datang !!!";
// });

// Route::get('blog', function (){
//     return view('blog');
// });

// Route::get('dosen','DosenController@index');

// Route::get('/pegawai/{nama}','App\Http\Controllers\PegawaiController@index');

// Route::get('/formulir','App\Http\Controllers\PegawaiController@formulir');

// Route::post('/formulir/proses','App\Http\Controllers\PegawaiController@proses');

// Route Blog
// Route::get('/blog2','App\Http\Controllers\BlogController@home');
// Route::get('/blog2/tentang','App\Http\Controllers\BlogController@tentang');
// Route::get('/blog2/kontak','App\Http\Controllers\BlogController@kontak');

// Route CRUD
Route::get('/pegawai','App\Http\Controllers\PegawaiController@index');
Route::get('/pegawai/tambah','App\Http\Controllers\PegawaiController@tambah');
Route::post('/pegawai/penyimpanan','App\Http\Controllers\PegawaiController@simpan');
Route::get('/pegawai/edit/{id}','App\Http\Controllers\PegawaiController@edit');
Route::post('/pegawai/update','App\Http\Controllers\PegawaiController@update');
Route::get('/pegawai/hapus/{id}','App\Http\Controllers\PegawaiController@hapus');
Route::get('/pegawai/cari','App\Http\Controllers\PegawaiController@cari');

// Route Validasi
Route::get('/input','App\Http\Controllers\PemalasController@input');
Route::post('/proses','App\Http\Controllers\PemalasController@proses');

// Route eloquont
Route::get('/pegawaii','App\Http\Controllers\pegawaiicontroller@index');