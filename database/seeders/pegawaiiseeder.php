<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use GuzzleHttp\Promise\Create;
use Illuminate\Support\Facades\DB;

class pegawaiiseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // data faker indonsia
        $faker=Faker::create('id_ID');

        // membuat dummy sebanyak 10 record
        for ($i=1; $i <=10 ; $i++) { 
            # insert data dummy pegawai dengan faker
            DB::table('pegawaii')->insert([
                'nama'=>$faker->name,
                'alamat'=>$faker->address
            ]);
        }
    }
}
