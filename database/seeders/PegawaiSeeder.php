<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // insert data melalui Faker
        $faker=Faker::create('id_ID');

        for ($b=1; $b <= 43 ; $b++) { 
            DB::table('pegawai')-> insert([
                'pegawai_nama'=>$faker->name,
                'pegawai_jabatan'=>$faker->jobTitle,
                'pegawai_umur'=>$faker->numberBetween(19,40),
                'pegawai_alamat'=>$faker->address
            ]);
        }
        

        // insert data ke table pegawai
        // DB::table('pegawai')-> insert([
        //     'pegawai_nama'=>'Jojo',
        //     'pegawai_jabatan'=>'Web Designer',
        //     'pegawai_umur'=>'48',
        //     'pegawai_alamat'=>'Jl. Mangga'
        // ]);
    }
}
