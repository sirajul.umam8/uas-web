<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class PegawaiController extends Controller{
    // disini isi controller pegawai

    // public function index ($name){
    //     return $name;
    // }

    // public function formulir(){
    //     return view('formulir');
    // }

    public function proses(Request $request){
        $nama = $request -> input('nama');
        $alamat = $request -> input('alamat');
        return "Nama : ".$nama . ", Alamat : ".$alamat;
    }

    public function index (){
        // Mengambil data dari table pegawai
        // $pegawai = DB::table('pegawai')->get();
        $pegawai=DB::table('pegawai')->paginate(10);

        // Mengirim data pegawai ke view index
        return view('index',['pegawai' => $pegawai]);
    }

    // Method untuk menampilkan view form tambah pegawai
    public function tambah(){
        // memanggil view tambah
        return view('tambahan');
    }

    // method untuk menyimpan data ke table
    public function simpan(Request $req){
        // insert data ke table pegawai yang ada di database
        DB::table('pegawai')->insert([
            'pegawai_nama' => $req->nama,
            'pegawai_jabatan'=> $req->jabatan,
            'pegawai_umur'=> $req->umur,
            'pegawai_alamat'=> $req->alamat
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/pegawai');

    }

    // method untuk edit data
    public function edit($id){
        // mengambil data pegawai berdasarkan id yang telah dipilih
        $pegawai1=DB::table('pegawai')->where('pegawai_id',$id)->get();
        

        // passing data pegawai yang didapat ke view edit
        return view('edit',['pegawai2'=>$pegawai1]);
    }

    // method untuk update data pada database
    public function update(Request $requ){
        // Update data pegawai
        DB::table('pegawai')->where('pegawai_id',$requ-> id)->update([
            'pegawai_nama'=> $requ->nama,
            'pegawai_jabatan'=>$requ->jabatan,
            'pegawai_umur'=>$requ->umur,
            'pegawai_alamat'=>$requ->alamat
        ]);
        // mengalihkan ke pegawai 
        return redirect('/pegawai');
        
    }
    
    // method untuk hapus data
    public function hapus($id){
        // menghapus data pegawai dari database berdasarkan id yang telah dipilih
        DB::table('pegawai')->where('pegawai_id',$id)->delete();

        // mengalihkan ke page pegawai
        return redirect('/pegawai');
    }

    // method untuk melakukan pencarian data
    public function cari(Request $request){
        // menangkap data pencarian
        $temukan=$request-> cari;

        // mengambil data dari table pegawai sesuai dengan pencarian data
        $pegawai=DB::table('pegawai')->where('pegawai_nama','like',"%".$temukan."%")->paginate();

        // mengirim data ke view index
        return view('index',['pegawai'=>$pegawai]);
    }
}