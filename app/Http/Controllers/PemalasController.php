<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PemalasController extends Controller
{
    // method menampilkan view
    public function input(){
        // menampilkan form input
        return view('input');
    }

    // method proses validasi
    public function proses(Request $permintaan){
        // mengubah peasan validasi
        $pesan=[
            'required' => ':attribute Isi goblok!!!',
            'min'=>':attribute datanya kurang :min lol',
            'max'=>':attribute datanya kelebihan lol',
            'numeric'=>':attribute ehhh pake angka anjreeet'
        ];

        $this->validate($permintaan,[
            'nama'=>'required|min:5|max:20',
            'pekerjaan'=>'required',
            'usia'=>'required|numeric'
        ],$pesan);

        

        
        return view('proses',['data'=> $permintaan]);
    }
}
