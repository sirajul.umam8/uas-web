<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class DosenController extends Controller{
    // disini isi controller dosen
    // public function index(){
    //     return "Hi, ini adalah method index, dalam controller
    //     DosenController.";
    // }

    // public function index(){
    //     return view('biodata');
    // }

    public function index(){
        $nama = "Moh. Sirajul Umam";
        $pelajaran =["Kalkulus","Trigonometri","Statistika"];
        return view('biodata', ['nama'=>$nama,'matkul'=>$pelajaran]);
    }
}