<?php

namespace App\Http\Controllers;

use App\Models\pegawai as ModelsPegawai;
use Illuminate\Http\Request;

 // memanggil  model pegawaii
 use App\Models\pegawai;


class pegawaiicontroller extends Controller{
    public function index()
    {
        // mengambil data pegawai
        // $pegawai=pegawai::all();

        // mengambil data pegawai yang pertama
        // $pegawai=pegawai::first();

        // mengambil data pegawai id
        $pegawai=pegawai::find(1);

        // mengirm ke view
        return view('eloquent/apapun',['pegawai'=>$pegawai]);
    }
}   